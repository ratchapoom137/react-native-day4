import { createStore, combineReducers } from 'redux';

import TodosReducer from './TodosReducer'
import CompletesReducers from './CompletesReducers'

const reducers = combineReducers({
    todos: TodosReducer,
    completes: CompletesReducers
})

const store = createStore(reducers);
const state = store.getState();
console.log(state)
export default store